var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser= require('body-parser');
var app = require('express')();

app.set('port', process.env.PORT || 3000);
var server = app.listen(5000, function() {
    console.log('Express server listening on port ' + server.address().port);
});
var handlebars=require('express3-handlebars').create({defaultLayout:'main'});
app.engine('handlebars',handlebars.engine);
app.set('view engine', 'handlebars');
// view engine setup
app.set('views', path.join(__dirname, 'views'));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());

var indexRouter = require('./routes/index');
var registerRouter=require('./routes/register');
var generalRouter=require('./routes/general');
var chatRouter=require('./routes/chat');


app.use('/', indexRouter);
app.use('/register',registerRouter);
app.use('/general/',generalRouter);
app.use('/chat',chatRouter);

app.use(express.static(path.join(__dirname, 'public')));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

module.exports = app;
