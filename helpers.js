function hbsHelpers(Handlebars) {
    var helpers = {
        link: function (obj) {
            var url = 'https://api.coingecko.com/api/v3/coins/list/';
            var id = obj.id;
            var name = obj.name;
            return new Handlebars.SafeString('<a href="' + url + id + '">' + name + '</a>');
        },
       times:function(obj)
        {
            for(var i=0;i<10;i++){
                return obj[i];
            }
        }
        // More helpers...
    }

    if (Handlebars && typeof Handlebars.registerHelper === "function") {
        for (var prop in helpers) {
            Handlebars.registerHelper(prop, helpers[prop]);
        }
    }
    else {
        return helpers;
    }
}
module.exports.register = hbsHelpers();
module.exports.helpers=hbsHelpers(null);