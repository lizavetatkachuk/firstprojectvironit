var express = require('express');
var router = express.Router();
var io=require('socket.io');
const jwt = require('jsonwebtoken');
var dbb=require('../sequelize');
const verifyToken=(req,res,next)=>
{
    jwt.verify(req.cookies['token'], 'chilipepper', (err) => {
        if (err) {
            res.status(200);
            //If error send Forbidden (403)
            res.render('index',{log: "You need to log in to visit the previous page"});
            r
        } else {
            //If token is successfully verified, we can send the autorized data
            next();
        }
    });
};
const checkToken = (err,req, res, next) => {

    const tok = req.cookies["token"];
    if(err){res.render('index',{log: "You need to log in to visit the previous page"});}
    if(typeof tok !== 'undefined') {
        req.token=tok;
        next();
    } else {
        res.render('index',{log: "You need to log in to visit the previous page"});
    }
};
/* GET home page. */
router.get('/',checkToken,verifyToken ,function(req, res, next) {
res.render('chat');
});

router.post('/',function(req,res,next)
{   var user=jwt.decode(req.cookies['token']);
    var email=user.email;
    var mes=req.body.msg;
    dbb.Chat.create({email: email, message: mes});
});
module.exports = router;