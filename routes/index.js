var express = require('express');
var router = express.Router();
var dbb=require('../sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index');
});

router.post('/',function(req,res)
{

    var pas=req.body.psw;
    var em=req.body.email;
    dbb.User.findOne({where:{email:em}}).
    then(user=>{
        if (!!user){
            bcrypt.compare(pas,user.passwords,function (err,response) {
                if(response)
                {
                    jwt.sign({user}, 'chilipepper', { expiresIn: '1h' },(err, token) => {
                        if(err) { console.log(err) }
                        res.cookie('token',token);
                        res.render('index',{log:"You've successfully logged in"});
                    });
                }

                else {
                    res.render('index', {wrongP: "Wrong password"})
                }

            }
        )}
        else res.render('index',{wrongE:"Wrong email"})

    })
});
module.exports = router;
