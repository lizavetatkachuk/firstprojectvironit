var express = require('express');
var router = express.Router();
var dbb=require('../sequelize');
const bcrypt = require('bcrypt');
/* GET register page. */
router.get('/', function(req, res) {
    res.render('register',{wrong:"  "});
});
router.post('/',function(req,res,next)
{
    var pas=req.body.psw;
    var pasc=req.body.psw1;
    var em=req.body.email;
    if(pas!==pasc){res.render('/register',{wrong:"Wrong password confirmation"});}
    else {
        dbb.sequelize
            .authenticate()
            .then(() => {
                console.log('Connection has been established successfully.');
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
            });
        bcrypt.hash(pas, 10, function (err, hash) {
            dbb.User.create({email: em, passwords: hash});
        })
        res.render('index');
    }
});

module.exports = router;
