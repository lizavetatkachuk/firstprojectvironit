var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');

const showReq=(req,res,next)=>{
    var request=require('request');
    var requestURL='https://api.coingecko.com/api/v3/coins/list';
      request(requestURL,function(response,body){
           var info=JSON.parse(body.body).slice(0,9);
           res.render('general',{currency:info});
            next();
        });

}
const verifyToken=(req,res,next)=>
{
    jwt.verify(req.cookies['token'], 'chilipepper', (err) => {
        if (err) {
            //If error send Forbidden (403)
            res.send("Problems with token verification");
            res.sendStatus(403);
        } else {
            //If token is successfully verified, we can send the autorized data
            next();
        }
    });
};
const checkToken = (req, res, next) => {

    const tok = req.cookies["token"];

    if(typeof tok !== 'undefined') {
        req.token=tok;
        next();
    } else {
         res.render('index',{log: "You need to log in to visit the previous page"});
    }
};
const showChart=(req,res,next)=>{
    var id=req.params.id;
    console.log(id);
    var request=require('request');
    var requestURL='https://api.coingecko.com/api/v3/coins/'+id+'/market_chart?vs_currency=usd&days=1';
    console.log(requestURL);
    request(requestURL,function(response,body){
        var info=JSON.parse(body.body);
        res.render('chart',{info:requestURL});
    });

}
/* GET home page. */
const checkVal=(req, res, next) => {

    const tok = req.cookies["token"];

    if(typeof tok !== 'undefined') {
        next();
    } else {
        //If header is undefined return Forbidden (403)
        res.sendStatus(403);
        res.send("Very bad situation. You are on the page, you're not allowed to be, cause you aren't registered. I'll fix it");
    }};
const getData=(req,res,next)=>
{
    var id=req.params.id;
    var request=require('request');
    var requestURL='https://api.coingecko.com/api/v3/coins/'+id+'?tickers=false&market_data=true&community_data=false&developer_data=false&sparkline=true';
    console.log(requestURL);
    request(requestURL,function(response,body){
        //res.send(body.body);
        var info=JSON.parse(body.body);

        res.render('currency',{currency:info}) ;
    });

}
router.get('/',showReq,function(req,res,next)  {

})
router.post('/',checkVal,showReq,function(req,res)  {
    res.clearCookie('token');
});
router.get('/:id', checkToken,verifyToken,getData);
router.get('/chart/:id',showChart);
module.exports = router;