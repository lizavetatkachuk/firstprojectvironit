const Sequelize = require('sequelize');
const DataTypes = require('sequelize');
 var sequelize = new Sequelize('crypto_db', 'root', 'rootroot', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        timestamps: false
    },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});
const User = sequelize.define('users', {
    email: {type: DataTypes.STRING,allowNull:false,unique:true,primaryKey:true },
    passwords:{type: DataTypes.STRING,allowNull:false}
})
const Chat=sequelize.define('messages',{
    email:DataTypes.STRING,
    message:{type:DataTypes.TEXT, allowNull:false}
})
const exp={
    User,
    sequelize,
    Chat
}
module.exports=exp;